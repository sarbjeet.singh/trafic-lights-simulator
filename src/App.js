import React from 'react'
import './lights.css';
import Simulator from './Simulator';
function App() {
  return (
    <div className="container">
      <Simulator/>
    </div>
  );
}

export default App;
