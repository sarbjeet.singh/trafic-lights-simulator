import React, { Component } from 'react';

import TrafficLights from '../TrafficLights'
export default class Simulator extends Component  {

  constructor(props){
    super(props);
    this.state = {
      activeLight:null
    }
  }

  componentWillUnmount(){
    clearInterval(this.intervalId);
  }

  handleStartSimulation = ()=>{
    this.setState({activeLight:"red"},()=>{
      this.intervalId = setInterval(()=>{
        this.startSimulation()
      },10000)
    })
  }

  handleStopSimulation = ()=>{
    this.setState({activeLight:null},()=>{
      clearInterval(this.intervalId);
    })
  }
 
  startSimulation = () => {
    const { activeLight } = this.state;
    switch(activeLight){
      case null:
        this.setState({activeLight:"red"})
        break;
      case "yellow":
        this.setState({activeLight:"green"})
        break;
      case "green":
        this.setState({activeLight:"red"})
        break;
      case "red":
        this.setState({activeLight:"yellow"})
        break;
      default:
        this.setState({activeLight:null})  
    }
  }
  render(){
    const {activeLight } = this.state;
    return(
      <div className="simulator">
        <div className="heading">Traffic Lights Simulator</div>
      <>
      <TrafficLights activeLight={activeLight}/>
      <div className="actions">
        <button onClick={this.handleStartSimulation}>Start</button>
        <button onClick={this.handleStopSimulation}>Stop</button>
      </div>
      </>
    </div>
    )
  }
}

 