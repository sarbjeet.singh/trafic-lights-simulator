import React from 'react';
import Circle from '../Circle';

const TrafficLights = ({activeLight}) => {
  return(
    <div className="trafic-light">
      <Circle color={activeLight==="red" ? "red" : null} />
      <Circle color={activeLight==="yellow" ? "yellow" : null} />
      <Circle color={activeLight==="green" ? "green" : null} />
    </div>
  )
}

export default TrafficLights