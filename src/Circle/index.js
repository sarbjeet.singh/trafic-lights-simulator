import React from 'react';

const Circle = ({color}) => {
  return(
    <div className="circle" style={{backgroundColor:color}}></div>
  )
}

export default Circle